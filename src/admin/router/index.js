import VueRouter from 'vue-router';
import store from '../store';
import homeRoutes from '../components/home/routes';
import dashboardRoutes from '../components/dashboard/routes';
import userRoutes from '../components/user/routes';
import timelineRoutes from '../components/timeline/routes';
import cardRoutes from '../components/card/routes';
import oauthRoutes from '../components/oauth/routes';

let routes = [
	{
		path: '/',
		redirect: 'home',
	},
	...homeRoutes,
	...dashboardRoutes,
	...userRoutes,
	...timelineRoutes,
	...cardRoutes,
	...oauthRoutes
];

let router = new VueRouter({
	//mode: 'history', // FIXME: Should be history on production
	base: window.location.pathname,
	routes
});

router.beforeEach((to, from, next) => {
	// Reset application current lang
	store.commit('lang/reset');

	// Route requires authentication
	if (to.matched.some(record => record.meta.requiresAuth)) {
		// User is already logged in
		if (store.getters['oauth/isLoggedIn']) {
			let token = JSON.parse(localStorage.getItem('token'));

			// Token has expired
			if (new Date(token.expire_date) < new Date()) {
				// Refresh access token
				store.dispatch('oauth/refreshGrant', token)
					.then(response => {
						// Set new access token then continue
						store.commit('oauth/setToken', response.data);
						next();
					}, response => {
						// Handle error then redirect to auth
						store.commit('mushi/logger/handleError', response);
						next({ path: '/home' })
					});
			} else {
				// Token is valid then continue
				next();
			}
		} else {
			// User is not logged in then redirect to auth
			next({ path: '/home' });
		}
	} else {
		// Route is public then continue
		next();
	}
});

export default router;
