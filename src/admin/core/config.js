import store from '../store';

export const getHeaders = function () {
	const token = JSON.parse(localStorage.getItem('token'));

	const headers = {
		'Accept': 'application/json',
		'Accept-Language': store.state.lang.current,
		'Authorization': 'Bearer ' + token.access_token
	}

	return headers;
};

export default {
	lang: {
		default: 'br'
	}
};
