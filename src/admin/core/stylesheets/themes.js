export default {
	'mushi': {
		primary: {
			color: 'blue-grey',
			hue: '900'
		},
		accent: {
			color: 'amber',
			hue: 'A400'
		},
		warn: 'red'
	},
	'mushi-darker': {
		primary: {
			color: 'light-blue',
			hue: 600
		},
		accent: {
			color: 'orange',
			hue: 800
		},
		warn: {
			color: 'red',
			hue: 700
		}
	},
	'mushi-green': {
		primary: 'green',
		accent: 'orange',
		warn: 'red'
	},
	'mushi-red': {
		primary: 'red',
		accent: 'orange',
		warn: 'red'
	},
	'mushi-gold': {
		primary: 'orange',
		accent: 'orange',
		warn: 'red'
	}
}
