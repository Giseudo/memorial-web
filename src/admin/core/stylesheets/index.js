import VueMaterial from 'vue-material';
import themes from './themes';

export default function install(Vue) {
	Vue.use(VueMaterial);

	// Register Vue Material theme
	Vue.material.registerTheme(themes);

	// Set default theme
	Vue.material.setCurrentTheme('mushi');
}
