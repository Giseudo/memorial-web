export default {
	title: 'Mídia | Mídias',
	image: {
		title: 'Imagem | Imagens'
	},
	document: {
		title: 'Documento | Documentos'
	},
	audio: {
		title: 'Audio | Audios'
	}
}
