export default {
	title: 'Autorização',
	client: {
		title: 'Cliente | Clientes',
		fieldset: {
			name: 'Nome',
			id: 'Client ID',
			secret: 'Client Secret',
			generate: 'Gerar @:oauth.client.fieldset.secret'
		}
	},
	scope: {
		title: 'Escopo | Escopos',
		fieldset: {
			id: 'Identificador',
			description: 'Descrição'
		}
	}
}
