export default {
	title: 'Autênticação',
	login: {
		title: 'Login',
		submit: 'Entrar',
		forgot: 'Esqueceu sua senha?'
	},
	logout: {
		title: 'Sair'
	},
	forgot: {
		title: 'Esqueci minha senha',
		submit: 'Enviar',
		login: 'Já possui uma conta? Entre aqui!'
	}
}
