export default {
	title: 'Card | Cards',
	all: 'Todos os Cards',
	fieldset: {
		titulo: 'Título',
		subtitulo: 'Subtitulo',
		slug: 'Slug',
		texto: 'Texto',
		descricao: 'Descrição',
		descricao_data: 'Descrição da Data'
	},
	opening: {
		title: 'Card de Abertura | Cards de Abertura',
		fieldset: {
			thumbnail: 'Miniatura dentro do card',
			url: 'URL do Vídeo (YouTube)'
		}
	},
	closing: {
		title: 'Card de Fechamento | Cards de Fechamento'
	},
	simple: {
		title: 'Card Simples | Cards Simples'
	},
	extra: {
		title: 'Card Extra | Cards Extras'
	},
	outer: {
		title: 'Card Externo | Cards Externos'
	},
	complex: {
		title: 'Card Complexo | Cards Complexos'
	}
}
