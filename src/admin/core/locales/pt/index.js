import model from './model';
import auth from './auth';
import user from './user';
import form from './form';
import action from './action';
import media from './media';
import card from './card';
import oauth from './oauth';
import setting from './setting';
import timeline from './timeline';

export default {
	model,
	auth,
	user,
	form,
	action,
	media,
	card,
	oauth,
	setting,
	timeline
}
