export default {
	title: 'Linha do Tempo | Linhas do Tempo',
	info: 'Detalhes da Linha do Tempo',
	fieldset: {
		title: 'Título',
		slug: 'Slug',
		begin: 'Início',
		end: 'Fim',
		calling: 'Chamada de Destaque na Home',
		bibliography: 'Bibliografia',
		background: 'Background',
	}
}
