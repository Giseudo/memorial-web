export default {
	title: 'Usuário | Usuários',
	fieldset: {
		title: '@:user.title',
		name: 'Nome',
		email: 'Email',
		password: {
			title: 'Editar Senha',
			first: 'Senha',
			second: 'Confirmação'
		}
	}
}
