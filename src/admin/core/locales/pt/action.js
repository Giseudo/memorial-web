export default {
	add: '{0} {1} criado com sucesso.',
	edit: '{0} {1} editado com sucesso.',
	delete: '{0} {1} removido com sucesso.',
	restore: '{0} {1} restaurado com sucesso.'
}
