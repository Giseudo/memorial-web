import moment from 'moment';
import VueI18n from 'vue-i18n';
import en from './en';
import pt from './pt';

let locales = { en, pt };

export default function install(Vue) {
	// Add dependencies
	Vue.use(VueI18n);

	// Set current lang
	Vue.config.lang = 'pt';

	moment.locale('pt-br');

	// Set locales
	Object.keys(locales).forEach(function (lang) {
		Vue.locale(lang, locales[lang])
	});
}
