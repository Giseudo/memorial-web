import VueResource from 'vue-resource';
import NProgress from 'nprogress';

export default function install(Vue) {
	Vue.use(VueResource);

	// VueResource Request interceptors
	Vue.http.interceptors.push((request, next) => {
		// Setup the progress bar
		NProgress.start();
		next(response => {
			NProgress.done();
			return response;
		});
	});
}
