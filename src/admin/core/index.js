import VueRouter from 'vue-router';
import VueMushi from 'vue-mushi';
import VueBus from 'vue-bus';

import Stylesheets from './stylesheets';
import Validate from './validate';
import Http from './http';
import Locales from './locales';
import Filters from './filters';
import Shared from '../shared';
import Directives from '../directives';

export default function install(Vue) {
	if (install.installed) {
		console.warn('Admin core is already installed.');
		return;
	}

	install.installed = true;

	Vue.use(VueRouter);
	Vue.use(VueMushi);
	Vue.use(VueBus);

	// Add core modules
	Vue.use(Stylesheets);
	Vue.use(Validate);
	Vue.use(Http);
	Vue.use(Locales);
	Vue.use(Shared);
	Vue.use(Filters);
	Vue.use(Directives);
}
