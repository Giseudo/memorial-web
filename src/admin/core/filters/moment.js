import moment from 'moment';

export default (value, format) => {
	if (!value)
		return moment().fromNow();
	else
		return moment(value).fromNow();
}
