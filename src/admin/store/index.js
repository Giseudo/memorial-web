import Vue from 'vue';
import Vuex from 'vuex';
import oauth from './modules/oauth';
import user from './modules/user';
import timeline from './modules/timeline';
import card from './modules/card';
import sidenav from './modules/sidenav';
import lang from './modules/lang';

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		oauth,
		user,
		timeline,
		card,
		sidenav,
		lang
	}
})
