import Vue from 'vue';

import { API_URL } from '../../../core/env';
import { getHeaders } from '../../../core/config';

export default {
	namespaced: true,
	state: {
		//
	},
	actions: {
		getTimelines (context, payload) {
			return Vue.http.get(API_URL + '/timelines',
				{ headers: getHeaders() }
			);
		},
		addTimeline (context, payload) {
			return Vue.http.post(API_URL + '/timelines',
				{
					timeline: payload.timeline,
					opening: payload.opening
				},
				{ headers: getHeaders() }
			)
		},
		getTimeline (context, payload) {
			return Vue.http.get(API_URL + '/timelines/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		editTimeline (context, payload) {
			return Vue.http.put(API_URL + '/timelines/' + payload.timeline.id,
				{
					timeline: payload.timeline,
					opening: payload.opening
				},
				{ headers: getHeaders() }
			);
		},
		deleteTimeline (context, payload) {
			return Vue.http.delete(API_URL + '/timelines/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		nextPage(context, payload) {
			return Vue.http.get(payload.url);
		}
	},
	getters: {
		//
	},
	mutations: {
		//
	}
}
