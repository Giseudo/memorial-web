import Vue from 'vue';
import { API_URL, CLIENT_ID, CLIENT_SECRET, CLIENT_SCOPE } from '../../../core/env';
import { getHeaders } from '../../../core/config';
import router from '../../../router';
import store from '../../../store';

import client from './client';
import scope from './scope';

export default {
	namespaced: true,

	modules: {
		client,
		scope
	},

	state: {
		isLoggedIn: false
	},

	getters: {
		/**
		 * Verify if user is logged in. Update data into state when called.
		 * @returns {Boolean}
		 */
		isLoggedIn (state) {
			var user = JSON.parse(localStorage.getItem('user'));
			var token = JSON.parse(localStorage.getItem('token'));

			// If user is logged in, populate authUser state with localStorage data
			if (user && token)
				state.isLoggedIn = true;

			return state.isLoggedIn;
		},

		/**
		 * Get current authenticated user
		 * @returns {Object}
		 */
		getUser (state) {
			return JSON.parse(localStorage.getItem('user'));
		},

		/**
		 * Get current token data
		 * @returns {Object}
		 */
		getToken (state) {
			return JSON.parse(localStorage.getItem('token'));
		}
	},

	actions: {
		/**
		 * Create access token using password grant type.
		 * @param {string} email - The user email.
		 * @param {string} password - The user password.
		 * @returns {Promise} A promise with the access and refresh token data.
		 */
		passwordGrant (context, payload) {
			return Vue.http.post(API_URL + '/oauth/access_token', {
				grant_type: 'password',
				client_id: CLIENT_ID,
				client_secret: CLIENT_SECRET,
				scope: CLIENT_SCOPE,
				username: payload.email,
				password: payload.password
			});
		},

		/**
		 * Create access token using refresh token grant type.
		 * @param {string} email - The user email.
		 * @param {string} password - The user password.
		 * @returns {Promise} A promise with the access and refresh token data.
		 */
		refreshGrant (context, payload) {
			return Vue.http.post(API_URL + '/oauth/access_token', {
				grant_type: 'refresh_token',
				client_id: CLIENT_ID,
				client_secret: CLIENT_SECRET,
				scope: CLIENT_SCOPE,
				refresh_token: payload.refresh_token
			});
		},

		/**
		 * Get the authenticated user.
		 * @returns {Promise} A promise with the authenticated user data.
		 */
		getAuthUser () {
			return Vue.http.get(API_URL + '/oauth/auth', {
				headers: getHeaders()
			});
		}
	},

	mutations: {
		/**
		 * Update token data on localStorage and state.
		 * @param {string} access_token - The access token.
		 * @param {string} refresh_token - The refresh token.
		 */
		setToken (state, payload) {
			localStorage.setItem('token', JSON.stringify({
				access_token: payload.access_token,
				refresh_token: payload.refresh_token,
				expire_date: new Date (new Date().getTime() + (1000 * payload.expires_in))
			}));
		},

		/**
		 * Update authenticated user data on localStorage and state.
		 * @param {string} email - The user email.
		 * @param {string} name - The user name.
		 * @param {string} role - The user privileges.
		 */
		setUser (state, payload) {
			localStorage.setItem('user', JSON.stringify({
				id: payload.id,
				email: payload.email,
				nome: payload.nome,
				role: payload.role
			}));
		},

		/**
		 * Authenticate and redirect the user.
		 * @param {string} email - The user email.
		 * @param {string} password - The user password.
		 */
		login (state, payload) {
			// Get the token
			store.dispatch('oauth/passwordGrant', payload)
				.then(response => {
					// Save token on local storage then authenticate user
					store.commit('oauth/setToken', response.data);
					store.commit('oauth/authenticate');
				}, response => Vue.$logger.error('Email or password does not match.'));
		},

		/**
		 * Logout and redirect user to login page.
		 */
		logout (state, payload) {
			// Reset states
			state.isLoggedIn = false;
			localStorage.removeItem('token');
			localStorage.removeItem('user');

			// Redirect to login page
			router.push('/home');
		},

		/**
		 * Register and authenticate the user.
		 * @param {string} email - The user email.
		 * @param {string} password - The user password.
		 */
		register (state, payload) {
			//
		},

		authenticate (state, payload) {
			// Get authenticated user
			store.dispatch('oauth/getAuthUser')
				.then(response => {
					// If no error, redirect to dashboard
					if (response.status == 200) {
						// Save data into local storage
						store.commit('oauth/setUser', response.data);

						// User is now logged in
						state.isLoggedIn = true;

						// Display success message
						Vue.$logger.success('Logged in. Welcome!');

						// Redirect user to entry page
						router.push('/dashboard');
					}

				}, response => Vue.$logger.handle(response));
		},
	}
}
