import Vue from 'vue';

import { API_URL } from '../../../core/env';
import { getHeaders } from '../../../core/config';

export default {
	namespaced: true,
	state: {
		//
	},
	actions: {
		getScopes (context, payload) {
			return Vue.http.get(API_URL + '/oauth/scopes',
				{ headers: getHeaders() }
			);
		},
		addScope (context, payload) {
			return Vue.http.post(API_URL + '/oauth/scopes',
				{ scope: payload },
				{ headers: getHeaders() }
			)
		},
		getScope (context, payload) {
			return Vue.http.get(API_URL + '/oauth/scopes/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		editScope (context, payload) {
			return Vue.http.put(API_URL + '/oauth/scopes/' + payload.id,
				{ scope: payload },
				{ headers: getHeaders() }
			);
		},
		deleteScope (context, payload) {
			return Vue.http.delete(API_URL + '/oauth/scopes/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		nextPage(context, payload) {
			return Vue.http.get(payload.url);
		}
	},
	getters: {
		//
	},
	mutations: {
		//
	}
}
