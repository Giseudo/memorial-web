import Vue from 'vue';

import { API_URL } from '../../../core/env';
import { getHeaders } from '../../../core/config';

export default {
	namespaced: true,
	state: {
		//
	},
	actions: {
		getClients (context, payload) {
			return Vue.http.get(API_URL + '/oauth/clients',
				{ headers: getHeaders() }
			);
		},
		addClient (context, payload) {
			return Vue.http.post(API_URL + '/oauth/clients',
				{ client: payload },
				{ headers: getHeaders() }
			)
		},
		getClient (context, payload) {
			return Vue.http.get(API_URL + '/oauth/clients/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		editClient (context, payload) {
			return Vue.http.put(API_URL + '/oauth/clients/' + payload.id,
				{ client: payload },
				{ headers: getHeaders() }
			);
		},
		deleteClient (context, payload) {
			return Vue.http.delete(API_URL + '/oauth/clients/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		nextPage(context, payload) {
			return Vue.http.get(payload.url);
		}
	},
	getters: {
		//
	},
	mutations: {
		//
	}
}
