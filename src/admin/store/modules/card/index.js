import Vue from 'vue';

import { API_URL } from '../../../core/env';
import { getHeaders } from '../../../core/config';

export default {
	namespaced: true,
	state: {
		//
	},
	actions: {
		getCards (context, payload) {
			return Vue.http.get(API_URL + '/cards',
				{ headers: getHeaders() }
			);
		},
		getLatestCards (context, payload) {
			return Vue.http.get(API_URL + '/cards/latest',
				{ headers: getHeaders() }
			);
		},
		searchCards (context, payload) {
			return Vue.http.get(API_URL + `/cards/search?q=${payload.q}`,
				{ headers: getHeaders() }
			)
		},
		addCard (context, payload) {
			return Vue.http.post(API_URL + '/cards',
				{ card: payload.card },
				{ headers: getHeaders() }
			)
		},
		getCard (context, payload) {
			return Vue.http.get(API_URL + '/cards/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		editCard (context, payload) {
			return Vue.http.put(API_URL + '/cards/' + payload.card.id,
				{ card: payload.card },
				{ headers: getHeaders() }
			);
		},
		deleteCard (context, payload) {
			return Vue.http.delete(API_URL + '/cards/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		nextPage(context, payload) {
			return Vue.http.get(payload.url);
		}
	},
	getters: {
		//
	},
	mutations: {
		//
	}
}
