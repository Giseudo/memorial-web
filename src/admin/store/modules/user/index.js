import Vue from 'vue';

import { API_URL } from '../../../core/env';
import { getHeaders } from '../../../core/config';

export default {
	namespaced: true,
	state: {
		//
	},
	actions: {
		getUsers (context, payload) {
			return Vue.http.get(API_URL + '/users',
				{ headers: getHeaders() }
			);
		},
		addUser (context, payload) {
			return Vue.http.post(API_URL + '/users',
				{
					user: payload.user,
					password: payload.password
				},
				{ headers: getHeaders() }
			)
		},
		getUser (context, payload) {
			return Vue.http.get(API_URL + '/users/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		editUser (context, payload) {
			return Vue.http.put(API_URL + '/users/' + payload.user.id,
				{
					user: payload.user,
					password: payload.password
				},
				{ headers: getHeaders() }
			);
		},
		deleteUser (context, payload) {
			return Vue.http.delete(API_URL + '/users/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		nextPage(context, payload) {
			return Vue.http.get(payload.url);
		}
	},
	getters: {
		//
	},
	mutations: {
		//
	}
}
