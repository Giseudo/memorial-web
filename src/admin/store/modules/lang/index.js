import Vue from 'vue';
import config from '../../../core/config';

export default {
	namespaced: true,
	state: {
		current: config.lang.default
	},
	getters: {
		getCurrent (state) {
			return state.current;
		}
	},
	mutations: {
		setCurrent (state, payload) {
			state.current = payload;
		},
		reset (state, payload) {
			state.current = config.lang.default;
		}
	}
}
