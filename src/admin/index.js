import Vue from 'vue';
import Core from './core';
import App from './App';
import router from './router';
import store from './store';

Vue.use(Core);

new Vue({
	el: '#app',
	router,
	store,
	render: createEle => createEle(App)
});
