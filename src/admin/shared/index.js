import LangMenu from './lang/LangMenu';
import InputSearch from './input/InputSearch';

export default function install(Vue) {
	Vue.component('adm-lang-menu', LangMenu);
	Vue.component('adm-input-search', InputSearch);
}
