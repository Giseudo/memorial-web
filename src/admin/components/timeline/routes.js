import TimelineList from './TimelineList';
import TimelineForm from './TimelineForm';

export default [
	{
		path: '/timelines',
		component: TimelineList,
		meta: { requiresAuth: true }
	},
	{
		path: '/timelines/add',
		component: TimelineForm,
		meta: { requiresAuth: true }
	},
	{
		path: '/timelines/edit/:id',
		component: TimelineForm,
		meta: { requiresAuth: true }
	}
]
