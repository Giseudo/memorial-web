import clientRoutes from './oauth-client/routes';
import scopeRoutes from './oauth-scope/routes';

export default [
	...clientRoutes,
	...scopeRoutes
]
