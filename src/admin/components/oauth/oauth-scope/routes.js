import OAuthScopeList from './OAuthScopeList';
import OAuthScopeForm from './OAuthScopeForm';

export default [
	{
		path: '/oauth/scopes',
		component: OAuthScopeList,
		meta: { requiresAuth: true }
	},
	{
		path: '/oauth/scopes/edit/:id',
		component: OAuthScopeForm,
		meta: { requiresAuth: true }
	},
	{
		path: '/oauth/scopes/add',
		component: OAuthScopeForm,
		meta: { requiresAuth: true }
	}
]
