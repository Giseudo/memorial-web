import CardList from './CardList';

export default [
	{
		path: '/cards',
		component: CardList,
		meta: { requiresAuth: true }
	},
	{
		path: '/cards/:type',
		component: CardList,
		meta: { requiresAuth: true }
	},
	/*{
		path: '/cards/simple/edit/:id',
		component: CardSimpleForm,
		meta: { requiresAuth: true }
	}*/
]
