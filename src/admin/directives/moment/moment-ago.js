import moment from 'moment'

export default {
	inserted (el, binding) {
		let time = binding.value;

		el.innerHTML = moment(time).fromNow()

		if (!el.$momentAgoKey) {
			el.$momentAgoKey = '' + keyGenerate++;
			funs[el.$momentAgoKey] = () => {
				el.innerHTML = moment(time).fromNow();
			}
			runAutoUpdate();
		}
	},

	unbind (el) {
		stopAutoUpdate(el.$momentAgoKey);
	}
}

let intervalTimes = 0,
	keyGenerate = 1,
	funs = {};

let runAutoUpdate = () => {
	if (intervalTimes != 0) return; //only run once

	intervalTimes = setInterval(() => {
		for(let key in funs){
			funs[key]();
		}
	}, 1000)
}

let stopAutoUpdate = key => {
	if (!key) return;

	delete funs[key];

	if (isEmptyObj(funs)) {
		clearInterval(intervalTimes);
		intervalTimes = 0;
	}
}

let isEmptyObj = obj => {
	for (let key in obj) {
		return false;
	}
	return true;
}
