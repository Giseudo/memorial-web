import MomentAgo from './moment/moment-ago';

export default function install(Vue) {
	Vue.directive('moment-ago', MomentAgo);
}
