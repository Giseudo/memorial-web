import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import App from './App';

Vue.use(VueRouter);
Vue.use(VueResource);

let Docs = Vue.component('app', App);

Docs = new Docs({
	el: '#app'
});
