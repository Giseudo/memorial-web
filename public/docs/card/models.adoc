== Models

=== Base – `Card`

==== Propriedades
$table::
`card`

$fillable::
```
id
data
descricao_data
titulo
credito_foto_capa
slug
subtitulo
texto
intervalo_inicio
intervalo_fim
descricao_busca
```

$timestamps::
`true`

$appends::
```
titulo
texto
subtitulo
```

==== Relações
timeline::
Has one `Timeline`

background_file::
Has one `File`

capa_file::
Has one `File`

==== Atributos
titulo::
Retorna e modifica título do card no idioma atual

texto::
Retorna e modifica texto do card no idioma atual

subtitulo::
Retorna e modifica subtitulo do card no idioma atual

descricao::
Retorna e modifica descrição do card no idioma atual

descricao_data::
Retorna e modifica descrição da data do card no idioma atual

credito_foto_capa::
Retorna e modifica credito da foto de capa do card no idioma atual

==== Métodos
`all ()`::
Retorna coleção de *Card* de forma paginada.

`latest ($timeline)`::
Retona coleção dos últimos *Cards* criados/modificados. Filtrando pela timeline caso identificador seja passado.

`search ()`::
Retorna coleção de *Card* encontrados pela regra dos filtros de forma paginada.

`find ($id)`::
Retorna *Card* com o código passado.

=== Simples – `CardSimple`
Card com background amarelo

=== Extra – `CardExtra`
Paginação +
Tags +
Template

==== Atributos
title::
N/A

==== Métodos
get()::
N/A

=== Complexo – `CardComplex`
Card com background amarelo

==== Atributos
title::
N/A

==== Métodos
get()::
N/A

=== Externo – `CardOuter`
Links internos ou externos

==== Atributos
title::
N/A

==== Métodos
get()::
N/A

=== Abertura – `CardOpening`
Cards de abertura, possuem uma thumbnail exclusiva na lateral da página, devemos assegurar que cada *Timeline* possui apenas um card de abertura.
