= Memorial Docs

== Novo painel administrativo
Estamos seguindo uma ordem de prioridade para a entrega dos módulos. A ideia é focar nos tópicos mais importantes, deixando os menos úteis (como Jogo da Memória) para o final. Já que ainda será possível utilizar o gerenciador antigo para essas partes.

=== Ordem de prioridade dos módulos

* [line-through]#Autênticação#
* [line-through]#Usuários#
* *Timelines* -- Em desenvolvimento
* Cards
** Simples
** Externo
*** Extra
*** Estático
*** Dinâmico
* Configurações Gerais
* Tags
* Destaques
* Jogo da Memória
* Bronca Social

=== Data de estimativa pra conclusão dos principais módulos
N/A

== Modificação

=== Gerenciamento de cards
Hoje, para gerenciar cards é necessário navegar até o formulário de edição de alguma `timeline`. Podemos otimizar a experência do usuário facilitando o acesso do conteúdo.

Os cards passarão ser acessíveis através do menu principal (ou lateral). Sendo possível alternar entre diferentes filtros de busca: _Timeline_, _Tipo_, _Data_, etc.

=== Upload de arquivos (individual)
Imagino que estejamos usando a maior parte do nosso espaço de disco com arquivos, tornando a despesa mensal para manter o servidor cada vez maior. E apesar de hoje ser possível exclui-los através do gerenciador, o arquivo físico continua permanecendo no servidor; ou seja, *estamos acumulando lixo desde o início do projeto*.

Para reduzir o número de arquivos armazenados no servidor, decidimos optar por uma nova proposta: nos campos de arquivo, além de ser possível o usuário selecionar o arquivo de seu dispositivo, vamos também possibilitar o uso de arquivos já cadastrados anteriormente em toda a aplicação.

==== Filtro de busca

=== Itens extras e slideshows

=== Editor de texto
- Vamos manter a versão antiga do editor de texto;
- Um novo editor de texto (https://quilljs.com[QuillJS]) será adotado como padrão;
- O usuário terá a possibilidade atualizar o conteúdo para o novo editor manualmente.

=== Motor de busca
N/A

=== Tradução
- Definir comportamento dos campos com múltiplos idiomas

== Implementação

=== Dashboard

==== Widget: Linhas do Tempo

Exibir todas as Linhas do Tempo, informando número de total de cards (extras, externos, complexos e simples), como também um indicador do seu estado atual: construção (amarelo), visitação (azul) ou produção (verde).

== Descontinuação

=== Campos do banco de dados
Todos os campos de upload de imagens desnecessários, como:

- Badge de extras
- Imagem de card na mini timeline
- Imagem de card de abertura
