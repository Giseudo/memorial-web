import path from 'path';

export default {
	rootPath: path.join(__dirname, '../public'),
	publicPath: '/',
	indexPath: 'index.html',
};
