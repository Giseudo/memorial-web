import webpack from 'webpack';
import merge from 'webpack-merge';
import config from '../config';
import baseWebpackConfig from './base';
import HtmlWebpackPlugin from 'html-webpack-plugin';

export default merge(baseWebpackConfig, {
	devtool: 'cheap-module-eval-source-map',
	entry: {
		'admin/js/admin': [
			'webpack-hot-middleware/client',
			'./src/admin/index.js'
		]
	}
})
