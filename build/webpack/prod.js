import webpack from 'webpack';
import merge from 'webpack-merge';
import baseWebpackConfig from './base';
import CompressionPlugin from 'compression-webpack-plugin';

export default merge(baseWebpackConfig, {
	devtool: 'cheap-source-map',
	plugins: [
		new webpack.LoaderOptionsPlugin({
			minimize: true,
			debug: false
		}),
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				warnings: false
			},
			output: {
				comments: false
			},
			sourceMap: false
		}),
		new CompressionPlugin({
			asset: "[path].gz[query]",
			algorithm: "gzip",
			test: /\.js$/,
			threshold: 10240,
			minRatio: 0.8
		})
	]
})
