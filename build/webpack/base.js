import path from 'path';
import config from '../config';
import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';

const resolvePath = (dir) => {
	return path.resolve(__dirname, '..', '..', dir);
};

export default {
	entry: {
		'js/memorial': './src/index.js',
		'admin/js/admin': './src/admin/index.js'
	},
	output: {
		path: config.rootPath,
		publicPath: '/',
		filename: '[name].bundle.js',
	},
	resolve: {
		extensions: ['.js', '.vue', '.json'],
		alias: {}
	},
	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				exclude: '/node_modules',
				options: {
					loaders: {
						sass: [
							'vue-style-loader',
							'css-loader',
							'sass-loader',
							{
								loader: 'sass-resources-loader',
								options: {
									resources: resolvePath('src/admin/core/stylesheets/variables.scss')
								}
							}
						]
					}
				}
			},
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: '/node_modules',
				include: [
					resolvePath('src')
				]
			},
			{
				test: /\.json$/,
				use: ['json-loader']
			}
		]
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: 'src/index.html',
			title: 'Vue Mushi',
			inject: true,
			chunks: ['js/memorial']
		}),
		new HtmlWebpackPlugin({
			filename: 'admin/index.html',
			template: 'src/admin/index.html',
			title: 'Vue Mushi',
			inject: true,
			chunks: ['admin/js/admin']
		})
	]
};
